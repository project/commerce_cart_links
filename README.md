Commerce Cart Links
===============

INTRODUCTION
------------
Commerce Cart Links lets you construct links that can manipulate a customer's shopping cart using query parameters. It supports emptying or deleting any existing cart and then adding one or more products to the cart, redirecting to an optional landing page after processing.

An example case for this module is checkout links through Google Merchant Center, allowing ads to send a shopper directly to the checkout form with a cart containing the advertised product.

## Building cart links

All cart links are handled by the /cart-links route. Cart contents are manipulated according to what follows in the URL:

* At least one URL argument including a product variation ID and quantity to be added to the cart separated by a hyphen. For example, to add 2 of product variation 57 to the cart, the URL would be /cart-links/57-2. To add additional products to the cart, use multiple such URL arguments, e.g. /cart-links/57-2/384-1.
* Append the destination=redirect/url query parameter to redirect the customer to a specific URL after the cart link has been processed similar to the query parameter behavior on form submission. If this parameter is omitted, the customer will either be redirected to the shopping cart form.
* Append the store=# query parameter to force the use of a specific store when resolving or creating a new cart.
* Append the existing query parameter with the value of new to force the creation of a new cart for this link without affecting any other carts the user may already have.
* Append the existing query parameter with the value of empty to empty a customer's existing cart (if one exists) before adding any the new product(s) to it.
* Append the existing query parameter with the value of delete to delete a customer's existing cart(s) before processing the cart link, meaning a new cart will always be created.
If the existing query parameter is not set in the link, Commerce Cart Links will simply add the relevant product(s) to whatever cart is resolved for the user.

These parameters are designed to give you maximum control over how existing carts and / or new carts are treated prior to the add to cart operation. However, a multi-store site or a site that depends on users managing multiple carts may experience issues when a cart link bypasses the default store resolving process.