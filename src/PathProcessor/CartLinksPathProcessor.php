<?php

namespace Drupal\commerce_cart_links\PathProcessor;

use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a path processor to rewrite cart-links URLs.
 *
 * As the route system does not allow arbitrary amount of parameters convert
 * the product arguments to a query parameter on the request.
 */
class CartLinksPathProcessor implements InboundPathProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    if (str_starts_with($path, '/cart-links/') && !$request->query->has('products')) {
      $products = str_replace('/cart-links/', '', $path);
      if (!empty($products)) {
        $request->query->set('products', explode('/', $products));
      }
      return '/cart-links';
    }

    return $path;
  }

}
