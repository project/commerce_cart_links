<?php

namespace Drupal\commerce_cart_links\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a configuration form for Cart links settings.
 */
class CartLinksSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_cart_links.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_cart_links_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('commerce_cart_links.settings');

    $form['whitelist_urls'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Referer URL whitelist'),
      '#description' => $this->t("Enter one domain per line. Cart links will only be processed if your server can match the domain of the link's referer URL to one in this list. Leave blank to process links regardless of referer."),
      '#default_value' => $config->get('whitelist_urls'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('commerce_cart_links.settings');
    $config
      ->set('whitelist_urls', $form_state->getValue('whitelist_urls'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
