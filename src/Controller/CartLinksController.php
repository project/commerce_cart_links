<?php

namespace Drupal\commerce_cart_links\Controller;

use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_order\Resolver\OrderTypeResolverInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a cart links controller.
 */
class CartLinksController extends ControllerBase {

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * The cart provider.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * The order type resolver.
   *
   * @var \Drupal\commerce_order\Resolver\OrderTypeResolverInterface
   */
  protected $orderTypeResolver;

  /**
   * The Cart links configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * Constructs a new CartLinksController object.
   *
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   *   The cart manager.
   * @param \Drupal\commerce_cart\CartProviderInterface $cart_provider
   *   The cart provider.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\commerce_order\Resolver\OrderTypeResolverInterface $order_type_resolver
   *   The order type resolver.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher service.
   */
  public function __construct(CartManagerInterface $cart_manager, CartProviderInterface $cart_provider, RequestStack $request_stack, OrderTypeResolverInterface $order_type_resolver, ConfigFactoryInterface $config_factory, PathMatcherInterface $path_matcher) {
    $this->cartManager = $cart_manager;
    $this->cartProvider = $cart_provider;
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->orderTypeResolver = $order_type_resolver;
    $this->config = $config_factory->get('commerce_cart_links.settings');
    $this->pathMatcher = $path_matcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_cart.cart_manager'),
      $container->get('commerce_cart.cart_provider'),
      $container->get('request_stack'),
      $container->get('commerce_order.chain_order_type_resolver'),
      $container->get('config.factory'),
      $container->get('path.matcher')
    );
  }

  /**
   * Processes the cart links and redirects to the URL.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response to the cart page or the page specified in the
   *   destination parameter.
   */
  public function processCartLinks() {
    $query_params = $this->currentRequest->query->all();
    // Redirect URL.
    $redirect_url = $this->getRedirectUrl();
    // Current store.
    $store = NULL;
    if (isset($query_params['store']) && !empty($query_params['store'])) {
      $store_storage = $this->entityTypeManager()->getStorage('commerce_store');
      /** @var \Drupal\commerce_store\Entity\StoreInterface $store */
      $store = $store_storage->load($query_params['store']);
    }

    // If there are no order items to add to cart, redirects to a specified URL
    // and displays a message.
    if (!$typed_order_items = $this->prepareOrderItems()) {
      $this->messenger()->addWarning('There are no products to add to the cart.');
      return new RedirectResponse($redirect_url->toString());
    }

    // Process existing query parameter.
    if (isset($query_params['existing'])) {
      switch ($query_params['existing']) {
        // Force the creation of a new cart for this link without affecting
        // any other carts the user may already have.
        case 'new':
          foreach ($typed_order_items as $order_type_id => $order_items) {
            foreach ($order_items as $order_item) {
              $cart = $this->cartProvider->getCart($order_type_id, $store);
              if (!$cart) {
                $cart = $this->cartProvider->createCart($order_type_id, $store);
              }
              $this->cartManager->addOrderItem($cart, $order_item);
            }
          }
          break;

        // Empty a customer's existing cart (if one exists) before adding
        // any the new product(s) to it.
        case 'empty':
          foreach ($typed_order_items as $order_type_id => $order_items) {
            foreach ($order_items as $order_item) {
              $cart = $this->cartProvider->getCart($order_type_id, $store);
              if ($cart) {
                $this->cartManager->emptyCart($cart, FALSE);
              }
              else {
                $cart = $this->cartProvider->createCart($order_type_id, $store);
              }
              $this->cartManager->addOrderItem($cart, $order_item);
            }
          }
          break;

        // Delete a customer's existing cart(s) before processing the cart link,
        // meaning a new cart will always be created.
        case 'delete':
          foreach ($typed_order_items as $order_type_id => $order_items) {
            foreach ($order_items as $order_item) {
              $cart = $this->cartProvider->getCart($order_type_id, $store);
              if ($cart) {
                $cart->delete();
                $this->cartProvider->clearCaches();
              }
              $cart = $this->cartProvider->createCart($order_type_id, $store);
              $this->cartManager->addOrderItem($cart, $order_item);
            }
          }
          break;
      }
    }
    else {
      // If the existing query parameter is not set in the link, simply add
      // the relevant product(s) to whatever cart is resolved for the user.
      foreach ($typed_order_items as $order_type_id => $order_items) {
        foreach ($order_items as $order_item) {
          $cart = $this->cartProvider->getCart($order_type_id, $store);
          if (!$cart) {
            $cart = $this->cartProvider->createCart($order_type_id, $store);
          }
          $this->cartManager->addOrderItem($cart, $order_item);
        }
      }
    }

    return new RedirectResponse($redirect_url->toString());
  }

  /**
   * Checks access to the cart links page.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function checkAccess(AccountInterface $account) {
    $access = AccessResult::allowedIf($this->validateQueryParams())
      ->andIf(AccessResult::allowedIf($this->validateRefererUrl()))
      ->andIf(AccessResult::allowedIfHasPermission($account, 'view commerce cart links'));

    return $access;
  }

  /**
   * Validates query parameters.
   *
   * @return bool
   *   TRUE if the query parameters are valid, FALSE otherwise.
   */
  protected function validateQueryParams() {
    $query_params = $this->currentRequest->query->all();

    // Validate existing parameter.
    $valid_existing = FALSE;
    if (isset($query_params['existing']) && !empty($query_params['existing'])) {
      // If the existing parameter exists, it must be in the list
      // of supported operations.
      if (in_array($query_params['existing'], ['new', 'empty', 'delete'])) {
        $valid_existing = TRUE;
      }
    }
    else {
      $valid_existing = TRUE;
    }

    // Validate products parameter.
    $valid_products = TRUE;
    if (isset($query_params['products']) && !empty($query_params['products'])) {
      foreach ($query_params['products'] as $product_args) {
        $product_arg = explode('-', $product_args);
        // The product variant ID and quantity must not be empty
        // and must be integers.
        $product_variation_id = isset($product_arg[0]) && !empty($product_arg[0]) ? intval($product_arg[0]) : NULL;
        $quantity = isset($product_arg[1]) && !empty($product_arg[1]) ? intval($product_arg[1]) : NULL;
        if (!$product_variation_id || !$quantity || !is_int($product_variation_id) || !is_int($quantity)) {
          $valid_products = FALSE;
          break;
        }
      }
    }
    else {
      $valid_products = FALSE;
    }

    return $valid_products && $valid_existing;
  }

  /**
   * Validates referer URL.
   *
   * @return bool
   *   TRUE if the referer URL is whitelisted, FALSE otherwise.
   */
  protected function validateRefererUrl() {
    $whitelist_urls = $this->config->get('whitelist_urls');

    // If the whitelist URLs list is empty, all domains are allowed.
    if (empty($whitelist_urls)) {
      return TRUE;
    }

    // If the referer URL is empty, the request was made from a direct URL.
    $referer_url = $this->currentRequest->headers->get('referer');
    if (empty($referer_url)) {
      // @todo Should it be allowed?
      return TRUE;
    }

    return $this->pathMatcher->matchPath(
      parse_url($referer_url, PHP_URL_HOST),
      $whitelist_urls
    );
  }

  /**
   * Gets the redirect URL.
   *
   * @return \Drupal\Core\Url
   *   The redirect URL.
   */
  protected function getRedirectUrl() {
    $query_params = $this->currentRequest->query->all();

    if (isset($query_params['destination']) && !empty($query_params['destination'])) {
      $redirect_url = Url::fromUserInput($query_params['destination']);
    }
    else {
      $redirect_url = Url::fromRoute('commerce_cart.page');
    }

    return $redirect_url;
  }

  /**
   * Prepares order items.
   *
   * @return array
   *   An array of order items, keyed by order_type_id.
   */
  protected function prepareOrderItems() {
    $query_params = $this->currentRequest->query->all();
    /** @var \Drupal\commerce_product\ProductVariationStorageInterface $product_variation_storage */
    $product_variation_storage = $this->entityTypeManager()->getStorage('commerce_product_variation');

    $order_items = [];
    foreach ($query_params['products'] as $product_args) {
      [$product_variation_id, $quantity] = explode('-', $product_args);
      /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation */
      $product_variation = $product_variation_storage->load($product_variation_id);
      if ($product_variation) {
        $order_item = $this->cartManager->createOrderItem($product_variation, $quantity);
        $order_type_id = $this->orderTypeResolver->resolve($order_item);
        $order_items[$order_type_id][] = $order_item;
      }
    }

    return $order_items;
  }

}
