<?php

namespace Drupal\commerce_cart_links\Routing;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for Cart links routes.
 */
class CartLinksRouteSubscriber extends RouteSubscriberBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a CartLinksRouteSubscriber object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('commerce_cart_links.process_cart_links')) {
      // If the redirect module is installed, it will cause a 403 error,
      // so we need to disable normalization.
      if ($this->moduleHandler->moduleExists('redirect')) {
        $route->setDefault('_disable_route_normalizer', TRUE);
      }
    }
  }

}
